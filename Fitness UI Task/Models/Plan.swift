//
//  Plan.swift
//  Fitness UI Task
//
//  Created by Daniel Nesterenko on 20.07.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import Foundation

struct Session: Codable {
    let length: Int
    let quoteAuthor: String
    let quote: String
    let chapterName: String
    let chapter: Int
    let difficulty: String
}
