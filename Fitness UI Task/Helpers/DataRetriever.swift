//
//  DataRetriever.swift
//  Fitness UI Task
//
//  Created by Daniel Nesterenko on 20.07.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import Foundation

class DataRetriever {
    static let shared = DataRetriever()
    
    private init() {}
    
    public func getSessions() -> [Session]? {
        if let url = Bundle.main.url(forResource: "sessions", withExtension: "json") {
            do {
                let data = try Data(contentsOf: url)
                let decoder = JSONDecoder()
                let jsonData = try decoder.decode(SessionsData.self, from: data)
                return jsonData.array
            } catch {
                print("error:\(error)")
            }
        }
        return nil
    }
}
