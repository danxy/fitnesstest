//
//  PlanViewController.swift
//  Fitness UI Task
//
//  Created by Daniel Nesterenko on 20.07.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import UIKit

class PlanViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource {
   
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var chapterLabel: UILabel!
    @IBOutlet weak var chapterNameLabel: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.register(UINib(nibName: "SessionCollectionViewCell",  bundle: nil), forCellWithReuseIdentifier: "SessionCollectionViewCell")
    }
    
    private func updateBackgroudImageForChapter(_ chapter: Int) {
        switch chapter {
        case 1:
            updateBackgroundWithImage(UIImage(named: "chapter1_bg")!)
        case 2:
            updateBackgroundWithImage(UIImage(named: "chapter2_bg")!)
        case 3:
            updateBackgroundWithImage(UIImage(named: "chapter3_bg")!)
        default:
            updateBackgroundWithImage(UIImage(named: "chapter1_bg")!)
        }
    }
    
    private func updateBackgroundWithImage(_ image: UIImage) {
        UIView.transition(with: backgroundImageView,
        duration: 0.2,
        options: .transitionCrossDissolve,
        animations: { self.backgroundImageView.image = image },
        completion: nil)
    }
    
    @IBAction func infoButtonAction(_ sender: Any) {
        showAlertWith("Alert", message: "Im default alert")
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return DataRetriever.shared.getSessions()?.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SessionCollectionViewCell", for: indexPath) as? SessionCollectionViewCell else {
               return UICollectionViewCell()
           }
            cell.sessionNumber = indexPath.row + 1
            cell.data = DataRetriever.shared.getSessions()?[indexPath.row]

            cell.delegate = self
        
           return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        let cellData = DataRetriever.shared.getSessions()![indexPath.row]
        self.chapterLabel.text = "CHAPTER " + cellData.chapter.description
        self.chapterNameLabel.text = cellData.chapterName
        updateBackgroudImageForChapter(cellData.chapter)
    }

    
}

extension PlanViewController: SessionCollectionViewCellDelegate {
    func sessionCollectionViewCellDidItPressed() {
           showAlertWith("Alert", message: "Im default alert")
    }
}

extension PlanViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
            return 40
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
            return CGSize(width: 300, height: 300)
        }
        
        func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
            return UIEdgeInsets(top: 0, left: 20, bottom: 0, right: 20)
        }
}

