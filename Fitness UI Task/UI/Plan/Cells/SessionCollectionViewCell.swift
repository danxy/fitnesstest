//
//  SessionCollectionViewCell.swift
//  Fitness UI Task
//
//  Created by Daniel Nesterenko on 20.07.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import UIKit

protocol SessionCollectionViewCellDelegate {
    func sessionCollectionViewCellDidItPressed()
}

class SessionCollectionViewCell: UICollectionViewCell {
    
    public var delegate: SessionCollectionViewCellDelegate?
    public var data: Session? {
        didSet {
            updateUI()
        }
    }
    public var sessionNumber: Int?
    
    @IBOutlet weak var view: UIView!
    @IBOutlet private weak var sessionLabel: UILabel!
    @IBOutlet private weak var lengthLabel: UILabel!
    @IBOutlet private weak var difficultyLabel: UILabel!
    @IBOutlet private weak var quoteLabel: UILabel!
    @IBOutlet private weak var quoteAutorLabel: UILabel!
    @IBOutlet private weak var difficultyImage: UIImageView!
    
    private func updateUI() {
        guard let sessionData = data else {return}
        guard let sessionNumber = sessionNumber else {return}
        sessionLabel.text = "Session " + sessionNumber.description
        lengthLabel.text = sessionData.length.description + " min"
        difficultyLabel.text = sessionData.difficulty
        quoteLabel.text = sessionData.quote
        quoteAutorLabel.text = sessionData.quoteAuthor
        
        setupDifficultyImage()
        
    }

    private func setupDifficultyImage() {
        guard let sessionData = data else {return}
        switch sessionData.difficulty {
        case "Easy":
            self.difficultyImage.image = UIImage(named: "intensity_1_dark")
        case "Medium":
            self.difficultyImage.image = UIImage(named: "intensity_2_dark")
        case "Hard":
            self.difficultyImage.image = UIImage(named: "intensity_3_dark")
            
        default:
              self.difficultyImage.image = UIImage(named: "intensity_1_dark")
        }
    }
    
    @IBAction func didItButtonAction(_ sender: Any) {
        delegate?.sessionCollectionViewCellDidItPressed()
    }
}
