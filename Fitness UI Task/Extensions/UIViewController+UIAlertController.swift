//
//  UIViewController+UIAlertController.swift
//  Fitness UI Task
//
//  Created by Daniel Nesterenko on 20.07.2020.
//  Copyright © 2020 Db. All rights reserved.
//

import UIKit

extension UIViewController {
    
    public func  showAlertWith(_ title: String,
                               message: String) {
        
        let alert = UIAlertController(title: title,
                                      message: message,
                                      preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Ok",
                                      style: .default,
                                      handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
